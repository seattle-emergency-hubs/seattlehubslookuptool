# Seattle Emergency Hub Locator

Emergency Hub Locator is a web-based tool designed to help users find their nearest emergency hub. It's simple to use and runs directly in your browser.

## Features

- Enter your location to find the nearest emergency hub.
- Displays the nearest hub on a map.
- Provides contact information for the hub captain, if available.

Future enhancements will include the ability to display the user's "sector" and their nearest GMRS repeater.

## How to Use

1. Open your preferred web browser and navigate to our web app URL.
2. You'll see a simple entry field for your location. Enter your address or coordinates here.
3. Click on the 'Locate' button.
4. The app will display the nearest emergency hub on a map, along with the hub captain's contact information if available.

## License

This project is licensed under the terms of the MIT license.